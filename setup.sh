#!/usr/bin/env bash
WORKDIR="$( cd "$(dirname "$0")" ; pwd -P )"
PROJECT=qqualys
cd $WORKDIR

# Install virtualenv if required
echo "Setup started..."
[[ -d "${HOME}/.local/bin" ]] && PATH="${HOME}/.local/bin:${PATH}"
[[ -n "`which virtualenv`" ]] && VENV=$(which virtualenv) || VENV=''
if [[ $VENV == '' ]]; then
	pip3 install virtualenv
	VENV=$(which virtualenv)
fi
if [ ! -f "$VENV" ]; then
	echo "virtualenv command not found, please install: pip install virtualenv"
	exit 1
fi

# Install requirements
$VENV -q -p python3 venv
source venv/bin/activate
pip3 install -r requirements.txt
deactivate

# Completed
echo "Setup complete!"
echo -e "\n\"./qqualys.py -h\" for usage."
