# qqualys

CLI tool to deal with Qualys

# Setup
Customize the sample config.ini (called config.ini.example) file with your details (ask for creds to ISO), then check the --help usage. Lots of options, the examples only cover a few.

# Examples


To see an host's vulns:


    LOGLEVEL=DEBUG ./qqualys.py  -c ~/work/secrets/qualys_config.ini -r -H smtp-liv1


These are the lines of code I had to send to ignore my QIDs for my hosts:


    ./qqualys.py  -c ~/work/secrets/qualys_config.ini -I -q 38628,38599,38601 -H smtp-liv1 -C 'Ignoring to avoid older clients on Stanford doing the right thing' --action ignore



For example, in the following transaction, I report on current vulnerabilities detected on smtp-liv1.
Note that two of the QIDs doesn't show up from the latest scan but it was in the previous report.  At the same time, another one showed up twice, probably because it was already preignored (and thus reconfirmed as ignored), or maybe because it's on a different port.


    golfieri@MacBook-Pro-7:~/work/qqualys$ LOGLEVEL=DEBUG ./qqualys.py  -c ~/work/secrets/qualys_config.ini -r -H smtp-liv1
    DEBUG Resolving for: smtp-liv1
    DEBUG IP resolved: 204.63.229.196
    DEBUG Starting new HTTPS connection (1): qualysapi.qg2.apps.qualys.com:443
    DEBUG https://qualysapi.qg2.apps.qualys.com:443 "GET /api/2.0/fo/asset/host/vm/detection//?action=list&ips=204.63.229.196&show_igs=1 HTTP/1.1" 200 None
    INFO GET: https://qualysapi.qg2.apps.qualys.com/api/2.0/fo/asset/host/vm/detection//?action=list&ips=204.63.229.196&show_igs=1
    [{'LAST_FOUND_DATETIME': '2019-06-29T07:24:25Z',
      'PORT': 587,
      'PROTOCOL': 'tcp',
      'QID': 38628,
      'RESULTS': 'TLSv1.0 is supported',
      'SEVERITY': 3,
      'TYPE': 'Confirmed'},
     {'LAST_FOUND_DATETIME': '2019-06-29T07:24:25Z',
      'PORT': 22,
      'PROTOCOL': 'tcp',
      'QID': 38725,
      'RESULTS': 'Vulnerable SSH-2.0-OpenSSH_6.7p1 Debian-5+deb8u4 detected on port 22 over TCP.',
      'SEVERITY': 3,
      'TYPE': 'Potential'},
     {'LAST_FOUND_DATETIME': '2019-06-29T07:24:25Z',
      'PORT': None,
      'PROTOCOL': None,
      'QID': 38726,
      'RESULTS': 'Vulnerable SSH-2.0-OpenSSH_6.7p1 Debian-5+deb8u4 detected on port 22 over TCP.',
      'SEVERITY': 3,
      'TYPE': 'Potential'}]
      
I now ignore one QID:

    golfieri@MacBook-Pro-7:~/work/qqualys$ ./qqualys.py  -c ~/work/secrets/qualys_config.ini -I -q 38628 -H smtp-liv1 -C 'Ignoring to avoid older clients on Stanford doing the right thing' --action ignore


Note that I could ignore 3 qids in one shot:

    golfieri@MacBook-Pro-7:~/work/qqualys$ ./qqualys.py  -c ~/work/secrets/qualys_config.ini -I -q 38628,38599,38601 -H smtp-liv1 -C 'Ignoring to avoid older clients on Stanford doing the right thing' --action ignore
    INFO GET: https://qualysapi.qg2.apps.qualys.com/msp/ignore_vuln.php/?action=ignore&ips=204.63.229.196&comments=Ignoring to avoid older clients on Stanford doing the right thing&qids=38628,38599,38601
    ['The operation was successfully completed',
     '45000',
     '38601',
     '204.63.229.196',
     'smtp-liv1.stanford.edu',
     '45002',
     '38601',
     '204.63.229.196',
     'smtp-liv1.stanford.edu',
     '45001',
     '38628',
     '204.63.229.196',
     'smtp-liv1.stanford.edu']
     
Then report again.

    golfieri@MacBook-Pro-7:~/work/qqualys$ LOGLEVEL=DEBUG ./qqualys.py  -c ~/work/secrets/qualys_config.ini -r -H smtp-liv1
    DEBUG Resolving for: smtp-liv1
    DEBUG IP resolved: 204.63.229.196
    DEBUG Starting new HTTPS connection (1): qualysapi.qg2.apps.qualys.com:443
    DEBUG https://qualysapi.qg2.apps.qualys.com:443 "GET /api/2.0/fo/asset/host/vm/detection//?action=list&ips=204.63.229.196&show_igs=1 HTTP/1.1" 200 None
    INFO GET: https://qualysapi.qg2.apps.qualys.com/api/2.0/fo/asset/host/vm/detection//?action=list&ips=204.63.229.196&show_igs=1
    [{'LAST_FOUND_DATETIME': '2019-06-29T07:24:25Z',
      'PORT': 22,
      'PROTOCOL': 'tcp',
      'QID': 38725,
      'RESULTS': 'Vulnerable SSH-2.0-OpenSSH_6.7p1 Debian-5+deb8u4 detected on port 22 over TCP.',
      'SEVERITY': 3,
      'TYPE': 'Potential'},
     {'LAST_FOUND_DATETIME': '2019-06-29T07:24:25Z',
      'PORT': None,
      'PROTOCOL': None,
      'QID': 38726,
      'RESULTS': 'Vulnerable SSH-2.0-OpenSSH_6.7p1 Debian-5+deb8u4 detected on port 22 over TCP.',
      'SEVERITY': 3,
      'TYPE': 'Potential'}]
    
If I made a mistake and I now want to restore the ignore action, I revert the --action to be restore:

    golfieri@MacBook-Pro-7:~/work/qqualys$ ./qqualys.py  -c ~/work/secrets/qualys_config.ini -I -q 38628,38599,38601 -H smtp-liv1 -C 'Ignoring to avoid older clients on Stanford doing the right thing' --action restore
    INFO GET: https://qualysapi.qg2.apps.qualys.com/msp/ignore_vuln.php/?action=restore&ips=204.63.229.196&comments=Ignoring to avoid older clients on Stanford doing the right thing&qids=38628,38599,38601
    ['The operation was successfully completed',
     '12785',
     '38599',
     '204.63.229.196',
     'smtp-liv1.stanford.edu',
     '17388',
     '38628',
     '204.63.229.196',
     'smtp-liv1.stanford.edu',
     '44755',
     '38628',
     '204.63.229.196',
     'smtp-liv1.stanford.edu',
     '45001',
     '38628',
     '204.63.229.196',
     'smtp-liv1.stanford.edu',
     '17389',
     '38601',
     '204.63.229.196',
     'smtp-liv1.stanford.edu',
     '45000',
     '38601',
     '204.63.229.196',
     'smtp-liv1.stanford.edu',
     '45002',
     '38601',
     '204.63.229.196',
     'smtp-liv1.stanford.edu']
     
Then report again, to find TLS1.0 affecting all ports where SMTP is operating:
     
    golfieri@MacBook-Pro-7:~/work/qqualys$ LOGLEVEL=DEBUG ./qqualys.py  -c ~/work/secrets/qualys_config.ini -r -H smtp-liv1
    DEBUG Resolving for: smtp-liv1
    DEBUG IP resolved: 204.63.229.196
    DEBUG Starting new HTTPS connection (1): qualysapi.qg2.apps.qualys.com:443
    DEBUG https://qualysapi.qg2.apps.qualys.com:443 "GET /api/2.0/fo/asset/host/vm/detection//?action=list&ips=204.63.229.196&show_igs=1 HTTP/1.1" 200 None
    INFO GET: https://qualysapi.qg2.apps.qualys.com/api/2.0/fo/asset/host/vm/detection//?action=list&ips=204.63.229.196&show_igs=1
    [{'LAST_FOUND_DATETIME': '2019-06-29T07:24:25Z',
      'PORT': 25,
      'PROTOCOL': 'tcp',
      'QID': 38628,
      'RESULTS': 'TLSv1.0 is supported',
      'SEVERITY': 3,
      'TYPE': 'Confirmed'},
     {'LAST_FOUND_DATETIME': '2019-06-29T07:24:25Z',
      'PORT': 465,
      'PROTOCOL': 'tcp',
      'QID': 38628,
      'RESULTS': 'TLSv1.0 is supported',
      'SEVERITY': 3,
      'TYPE': 'Confirmed'},
     {'LAST_FOUND_DATETIME': '2019-06-29T07:24:25Z',
      'PORT': 587,
      'PROTOCOL': 'tcp',
      'QID': 38628,
      'RESULTS': 'TLSv1.0 is supported',
      'SEVERITY': 3,
      'TYPE': 'Confirmed'},
     {'LAST_FOUND_DATETIME': '2019-06-29T07:24:25Z',
      'PORT': 22,
      'PROTOCOL': 'tcp',
      'QID': 38725,
      'RESULTS': 'Vulnerable SSH-2.0-OpenSSH_6.7p1 Debian-5+deb8u4 detected on port 22 over TCP.',
      'SEVERITY': 3,
      'TYPE': 'Potential'},
     {'LAST_FOUND_DATETIME': '2019-06-29T07:24:25Z',
      'PORT': None,
      'PROTOCOL': None,
      'QID': 38726,
      'RESULTS': 'Vulnerable SSH-2.0-OpenSSH_6.7p1 Debian-5+deb8u4 detected on port 22 over TCP.',
      'SEVERITY': 3,
      'TYPE': 'Potential'}]