#!venv/bin/python
# -*- coding: utf-8 -*-

__author__ = "UIT ET IEDO - Stanford University <uit-et-eit-iedo-staff@office365stanford.onmicrosoft.com>"
__license__ = "Apache License 2.0"
"""
    qqualys
    ~~~~~~~~
    Interact with Stanford's Qualys
"""
import sys, os, yaml, argparse, datetime, socket, re, operator, configparser, requests
import logging

logging.basicConfig(
    stream=sys.stdout,
    level=os.environ.get("LOGLEVEL", "INFO"),
    format="%(levelname)s %(message)s",
)
logger = logging.getLogger(__name__)
from io import StringIO
from pprint import pprint
from lxml import objectify
import ipaddress


def is_ip_address(addr):
    try:
        socket.inet_aton(addr)
    except socket.error:
        return False
    return addr


def resolve_to_ip(host):
    logger.debug("Resolving for: %s", host)
    host = host.strip()
    if "," in host:
        listing = []
        for h in host.split(","):
            listing.append(str(resolve_to_ip(h)))
        return ",".join(listing)
    if host.endswith(".local"):
        return None
    try:
        ip_address = ipaddress.ip_address(host)
        logger.debug("IP passed: %s", host)
        return ip_address
    except:
        if "." not in host:
            host += ".stanford.edu"
        try:
            # the gethostbyname syscall returns a 3-ple, index 2 is array of IP(s)
            address = socket.getaddrinfo(host, None)[0][4][0]
            logger.debug("IP resolved: %s", address)
            return ipaddress.ip_address(address)
        except socket.gaierror:
            logger.debug("Can not resolve name: %s", host)
            return None


def get_pqdn(host):
    return host.lower().replace(".stanford.edu", "")


def get_fqdn(host):
    if "." in host:
        return host.lower()
    else:
        return host.lower() + ".stanford.edu"


class Qapi:
    vm_fields = [
        "TYPE",
        "SEVERITY",
        "QID",
        "RESULTS",
        "PORT",
        "PROTOCOL",
        "LAST_FOUND_DATETIME",
    ]
    hosts = []
    url = None
    cache = None
    auth = None
    headers = {
        "X-Requested-With": "Kite via qualysapi",
        "Accept": "application/xml",
        "Content-type": "text/xml",
    }
    """Wraps all the boring redundancy for Qualys api calls, e.g. the URI"""
    # user_login='nbfa@stanford.edu' # By default, all scans seem to be owned by Noah
    def __init__(
        self,
        config="qualys_config.ini",
        remember_me=False,
        remember_me_always=False,
        username=None,
        password=None,
        hostname=None,
        cache=None,
    ):
        conf = configparser.RawConfigParser()
        buffer = StringIO(config)
        conf.read_file(buffer)
        self.url = "https://" + conf.get("info", "hostname")
        self.auth = (conf.get("info", "username"), conf.get("info", "password"))
        self.cache = cache
        self._tag = "TCG"

    @property
    def tag(self):
        return self._tag

    @tag.setter
    def tag(self, value):
        self._tag = value

    def _make_get_params(self, params):
        if params:
            return "?" + "&".join(
                "{0}={1}".format(key, value) for key, value in params.items()
            )
        else:
            return ""

    def _request_output(self, response, raw_output):
        if response.status_code != 200:
            raise Exception("Error, response was {}".format(response.__dict__))
        if raw_output:
            return response.text
        else:
            return objectify.fromstring(response.text.encode("utf-8"))

    def _post_xml(self, uri, params, headers=None, raw_output=False, get_params_mode=False):
        if get_params_mode:
            url = self.url + uri + "/" + self._make_get_params(params)
            params={}
        else:
            url = self.url + uri
        headers = self.headers if not headers else headers
        response = requests.post(url, data=params, headers=headers, auth=self.auth)
        logging.info("POST: {}, params: {}, response: {}".format(url, params, response))
        return self._request_output(response, raw_output)

    def _get_xml(self, uri, params, headers=None, raw_output=False):
        url = self.url + uri + "/" + self._make_get_params(params)
        headers = self.headers if not headers else headers
        response = requests.get(url, headers=self.headers, auth=self.auth)
        logging.info("GET: {}".format(url))
        return self._request_output(response, raw_output)

    def print(self, sorted_list, vm_fields=None):
        vm_fields = vm_fields or self.vm_fields
        pprint(
            [dict((key, x.__dict__.get(key)) for key in vm_fields) for x in sorted_list]
        )

    def get_rest_api_version(self):
        return self._get_xml(
            "/qps/rest/portal/version/",
            None,
            headers={"Accept": "application/xml"},
            raw_output=True,
        )

    def ignore_vuln(
        self,
        host,
        qids,
        comments="DEFAULT comment from qqualys for ignoring",
        action="ignore",
        days_to_reopen=None,
    ):
        params = {
            "action": action,
            "ips": resolve_to_ip(host),
            "comments": comments,
            "qids": qids,
        }
        if days_to_reopen and 1 < days_to_reopen < 730:
            params.update({"reopen_ignored_days": days_to_reopen})
        return self._get_xml("/msp/ignore_vuln.php", params)

    def get_agents_by_tag_xml(self, tag):
        params = """<ServiceRequest>
             <filters>
                <Criteria field="tagName" operator="EQUALS">{tag}</Criteria>
             </filters>
            </ServiceRequest>""".format(
            tag=tag
        )
        return self._post_xml("/qps/rest/2.0/search/am/hostasset/", params)

    def get_asset_by_dns(self, hostname, operator="EQUALS"):
        params = """<ServiceRequest>
             <filters>
                <Criteria field="dnsHostName" operator="{operator}">{hostname}</Criteria>
             </filters>
            </ServiceRequest>""".format(
            hostname=hostname, operator=operator
        )
        return self._post_xml("/qps/rest/2.0/search/am/hostasset/", params)
        # Shows vulns: oxml.xpath('//vuln/list/*')

    def get_asset_details_xml(self, asset_id):
        if not asset_id.isdigit():
            asset_id = self.get_asset_id(asset_id)
        headers = {"Content-type": "text/xml"}
        return self._get_xml(
            "/qps/rest/2.0/get/am/hostasset/" + str(asset_id),
            None,
            headers={"Content-type": "text/xml"},
        )

    def get_asset_id(self, hostname):
        if hostname.isdigit():
            return int(hostname)
        else:
            if self.cache:
                asset_id = self.cache.get(hostname, "asset_id")
                if asset_id:
                    logger.info(
                        "%s was found in DynamoDB as asset_id: %s", hostname, asset_id
                    )
                    return int(asset_id)
            # else:
            oxml = self.get_asset_by_dns(get_fqdn(hostname))
            if not "data" in oxml.__dict__:
                oxml = self.get_asset_by_dns(get_pqdn(hostname), operator="CONTAINS")
            if oxml.count == 1:
                asset_id = oxml.xpath("//data/HostAsset/id")[0]
                logger.info("%s has HostAsset/id => %s", hostname, asset_id)
                if self.cache:
                    logger.info(
                        "Caching %s in DynamoDB as asset_id: %s", hostname, asset_id
                    )
                    self.cache.set(hostname, {"asset_id": asset_id})
                return int(asset_id)
        return None

    def get_sw_inventory(self, host):
        asset_id = self.get_asset_id(host)
        oxml = self.get_asset_details_xml(asset_id)
        software_list = {}
        for sw in oxml.xpath("//HostAssetSoftware"):
            software_list[sw.name.text.encode("utf-8")] = sw.version.text.encode(
                "utf-8"
            )
        return software_list

    def purge_host(self, host):
        params = {"action": "purge","ips": str(resolve_to_ip(host)), "echo_request":1}
        oxml = self._post_xml("/api/2.0/fo/asset/host", params, get_params_mode=True)
        return oxml.xpath("//*/text()")

    def populate_agent_hosts(self):
        oxml = self.get_agents_by_tag_xml(self.tag)
        self.hosts = oxml.xpath("//data/HostAsset/dnsHostName")
        self.hosts = [x.replace(".stanford.edu", "") for x in self.hosts]

    def start_new_scan(self, hostname):
        params = {
            "action": "launch",
            "scan_title": "TCG_scanning_for_" + hostname,
            "ip": resolve_to_ip(hostname),
            "option_title": "ISO Official 3/4/5 (Site-wide)",
            "default_scanner": 1,
        }
        # DO NOT ask me why, but somehow one day I realized start new scan is a post call that needs get style params???
        return self._post_xml(
            "/api/2.0/fo/scan/" + self._make_get_params(params),
            params={},
            raw_output=True,
        )

    def get_scans_list(self, username=None):
        if not username:
            username = self.auth[0]
        oxml = self._get_xml(
            "/api/2.0/fo/scan/",
            {"action": "list", "state": "Finished,Running,Queued,Loading"},
        )
        listing = []
        for s in oxml.xpath(
            '//RESPONSE/SCAN_LIST/SCAN[USER_LOGIN="{}"]'.format(username)
        ):
            listing.append(
                (s.TITLE, s.TARGET, s.LAUNCH_DATETIME, s.DURATION, s.STATUS.STATE)
            )
        logger.info("Found %d vulnerabilities", len(listing))
        return listing

    def get_scans_as_ul_li_list(self):
        listing = self.get_scans_list()
        array = []
        for item in listing:
            array += [
                "{0} ({1}): {2} [{4}]".format(
                    item[0], item[1], item[2], item[3], item[4]
                )
            ]
        return array

    def get_remediation_tickets(self, hostname):
        params = {"show_vuln_details": 1, "states": "OPEN"}
        if is_ip_address(hostname):
            params.update({"ips": hostname})
        else:
            params.update({"dns_contains": hostname})
        oxml = self._get_xml("/msp/ticket_list.php", params)
        tnow = datetime.datetime.now()
        # since "dns-contains" is not a exact match search):
        tickets = oxml.xpath(
            '//TICKET_LIST/TICKET[VULNINFO[SEVERITY>3]][DETECTION[DNSNAME[text() ="{0}.stanford.edu" or text()="{0}"]]]'.format(
                get_pqdn(hostname)
            )
        )
        results = []
        for t in tickets:
            tfound = datetime.datetime.strptime(
                str(t.CREATION_DATETIME), "%Y-%m-%dT%H:%M:%SZ"
            )
            age = tnow - tfound
            # Had to put a limit since some badly managed servers had so many chunky vulnerabilities to load, that they would hang Flask in composing the pages.
            diagnosis = (
                t.DETAILS.DIAGNOSIS.text.encode("utf-8") if len(tickets) < 15 else ""
            )
            solution = (
                t.DETAILS.SOLUTION.text.encode("utf-8") if len(tickets) < 15 else ""
            )
            results.append(
                (
                    int(t.VULNINFO.QID),
                    int(t.VULNINFO.SEVERITY),
                    t.VULNINFO.TITLE.text.encode("utf-8"),
                    t.HISTORY_LIST.HISTORY.SCAN.REF.text.encode("utf-8")[:5],
                    int(age.days),
                    t.DETAILS.CONSEQUENCE.text.encode("utf-8"),
                    diagnosis,
                    solution,
                )
            )
        logger.info("Found %d remediation tickets", len(results))
        return results

    def get_remediation_tickets_list(self, hostname):
        listing = self.get_remediation_tickets(get_pqdn(hostname))
        # sort by source first (a in agent comes first, but it's less important than VM ('v') so reversing
        listing = sorted(listing, key=operator.itemgetter(3, 1, 4), reverse=True)
        return listing

    # List all vulnerabilities found so far in previous scans
    def get_vulnerabilities_scan_list(self):
        params = {"action": "list", "state": "Finished"}
        self.request(params, "/api/2.0/fo/scan/")

    # Get host info about a specific IP address or hostname
    def get_host_vulnerabilities(self, host, extra_params=None):
        params = {"action": "list", "ips": str(resolve_to_ip(host)), "show_igs": 1}
        if extra_params:
            params.update(extra_params)
        oxml = self._get_xml("/api/2.0/fo/asset/host/vm/detection/", params)
        return oxml.xpath("//DETECTION[SEVERITY>2]")

    def get_host_ignored_vulnerabilities(self, host):
        extra_params = {"include_ignored": 1}
        return self.get_host_vulnerabilities(host, extra_params)

    def get_qid_info(self, qid):
        params = {"action": "list", "ids": qid, "details": "Basic", "echo_request": 1}
        oxml = self._get_xml("/api/2.0/fo/knowledge_base/vuln", params)
        return oxml

    def get_qid_summary_list(self, host):
        vulns = self.get_host_vulnerabilities(host)
        qid_list = ",".join([str(i[0]) for i in vulns])
        results = self.get_qid_info(qid_list)
        array = []
        if results:
            for row in results:
                data = row.getchildren()
                array += ["{0} ({1}): {2}".format(data[0], data[2], data[3])]
        return array


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # Works and tested
    parser.add_argument(
        "-c",
        "--config",
        action="store",
        dest="config_file",
        help="Qualys Config file full path",
        required=True,
    )
    parser.add_argument(
        "-H",
        "--host",
        action="store",
        dest="host",
        help="host target (can be FQDN, Stanford PQDN or IP address)",
    )
    parser.add_argument(
        "-l",
        "--list_scans",
        action="store_true",
        help="List all available vulnerabilities scans",
    )
    parser.add_argument(
        "-a",
        "--appliances-lookup-by-fwzone-tag",
        action="store",
        dest="appliance_lookup",
        help="Get appliances associated to given FW zone tag (e.g. FOA, FOA2, etc",
    )
    parser.add_argument(
        "--lookup", action="store", dest="lookup", help="Resolve to host Asset id"
    )
    parser.add_argument(
        "-s", "--scan", action="store_true", help="Scan the server specified with -H"
    )
    parser.add_argument(
        "-T",
        "--tickets",
        action="store_true",
        help="Search for remediation tickets for host specified by -H",
    )
    parser.add_argument(
        "-u",
        "--username",
        action="store",
        dest="username",
        help="username used for actions in Qualys",
    )
    parser.add_argument("--hostid", action="store", dest="hostid", help="Host Asset id")
    parser.add_argument(
        "-A",
        "--list_agents",
        action="store_true",
        help="EXPERIMENTAL List all available agents and their info",
    )
    parser.add_argument(
        "--show_software_info",
        action="store_true",
        help="Show software info for QCA host asset",
    )
    parser.add_argument(
        "-t", "--tag", dest="tag", help="List all available agents and their info"
    )
    parser.add_argument(
        "-r",
        "--report",
        action="store_true",
        help="report on -H host for known found vulnerabilities - high level",
    )
    parser.add_argument(
        "-Q",
        "--show_qca_asset_info",
        action="store_true",
        help="shows assets info gathered via QCA, needs -H",
    )
    parser.add_argument(
        "-P",
        "--purge_host",
        action="store_true",
        help="Purge all data for a given host, needs -H",
    )
    parser.add_argument(
        "-q",
        "--qids",
        action="store",
        dest="qids",
        help="QID info to show (can be comma separated no spaces or range e.g. 90023-90040)",
    )
    parser.add_argument(
        "--list_ignored_vulnerabilities",
        action="store_true",
        help="shows ignored vulnerabilities for a given host, needs -H",
    )
    parser.add_argument(
        "-I",
        "--ignore_vuln",
        action="store_true",
        help="Ignore vulnerability, needs -q -H and -C",
    )
    parser.add_argument(
        "-C",
        "--comment",
        action="store",
        dest="comment",
        help="Comment field for commands needing one, e.g. -I",
    )
    parser.add_argument(
        "-D",
        "--days_to_reopen",
        action="store",
        dest="days_to_reopen",
        help="(optional) Days to reopen ignored vulnerability (1-730)",
    )
    parser.add_argument(
        "--action",
        action="store",
        dest="action",
        help="Action to use when ignoring with -I (ignore, restore)",
    )
    parser.add_argument(
        "-i", "--interactive", action="store_true", help="don't return, go into Ipython"
    )
    parser.add_argument(
        "-V",
        "--version",
        action="store_true",
        help="Show rest API version supported by server",
    )
    args = parser.parse_args()

    config = None
    with open(args.config_file, "r") as f:
        config = f.read()
    q = Qapi(config=config)

    if args.version:
        print(q.get_rest_api_version())
    elif args.lookup:
        print(q.get_asset_id(args.lookup))
    elif args.list_agents and args.tag:
        print(q.get_agents_by_tag_xml(args.tag))
    elif args.show_software_info and (args.hostid or args.host):
        if args.hostid:
            print(q.get_sw_inventory(args.hostid))
        else:
            print(q.get_sw_inventory(args.host))
    elif args.list_scans:
        print(q.get_scans_list(args.username))
    elif args.host:
        if args.scan:
            print("Starting new scan")
            print(q.start_new_scan(args.host))
        elif args.tickets:
            sorted_list = q.get_remediation_tickets_list(args.host)
        elif args.report:
            q.print(q.get_host_vulnerabilities(args.host))
        elif args.ignore_vuln:
            oxml = q.ignore_vuln(
                args.host, args.qids, args.comment, args.action, args.days_to_reopen
            )
            pprint(oxml.xpath("//*/text()"))
        elif args.purge_host:
            print(q.purge_host(args.host))
        elif args.show_qca_asset_info:
            print(q.get_asset_details_xml(args.host))
        elif args.list_ignored_vulnerabilities:
            q.print(
                q.get_host_ignored_vulnerabilities(args.host),
                [
                    "TYPE",
                    "SEVERITY",
                    "QID",
                    "RESULTS",
                    "PORT",
                    "PROTOCOL",
                    "LAST_FOUND_DATETIME",
                    "IS_IGNORED",
                ],
            )
        else:  # Just args.host
            print("The -H option cannot be used alone")
    elif args.qids:
        sorted_list = q.get_qid_info(args.qids)
        pprint(sorted_list.xpath("//RESPONSE/VULN_LIST/VULN//*[text()]"))
    else:
        pass

    if args.interactive:
        import IPython

        IPython.embed()  #### import IPython; IPython.embed()
